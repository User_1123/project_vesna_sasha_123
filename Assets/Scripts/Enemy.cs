using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;
using static UnityEditor.PlayerSettings;

public class Enemy : MonoBehaviour
{
    public float Health = 100f;
    public float Speed = 4f;
    public Gun gun;


    void Update()
    {
        Vector3 Direction = Speed < 0f ? Vector3.left : Vector3.right;

        RaycastHit hit;
        Physics.Raycast(transform.position, Direction, out hit, 10f);
        if (hit.transform != null && hit.transform.tag == "Player")
            gun.Shoot();
        else Move(Speed);

        if (IsCliffOrWall(Direction))
        {
            transform.Rotate(0, 180, 0);
            Speed = -Speed;
        }

        if (Health < 0)
            Dead();
    }

    void Move(float SpeedMove)
    {
        transform.position += new Vector3(SpeedMove * Time.deltaTime, 0, 0);
    }

    void Dead()
    {
        Destroy(gameObject);
    }

    bool IsCliffOrWall(Vector3 Direction)
    {
        return
            Physics.Raycast(transform.position, Direction, transform.localScale.x * 0.5f) ||
            !Physics.Raycast(transform.position + transform.localScale.x * 0.5f * Direction, Vector3.down, transform.localScale.y * 0.5f);
    }

    void OnCollisionStay(Collision collisionInfo)
    {
        var lava = collisionInfo.gameObject.GetComponent<Lava>();
        var bullet = collisionInfo.gameObject.GetComponent<Bullet>();
        if (lava)
            Health -= lava.DamagePerSecond * Time.fixedDeltaTime;

        if (bullet)
            Health -= bullet.hitDamage;
        return;
        //OnCollisionStay called during physics update, using fixedDeltaTime
    }
}
