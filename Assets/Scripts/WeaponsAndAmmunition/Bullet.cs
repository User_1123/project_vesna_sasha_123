using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    public float hitDamage = 5f;
    public float Velosity = 20f;
    public float LiveTime;

    void OnCollisionStay(Collision collisionInfo)
    {
        BulletDestroy();
    }

    void Update()
    {
        LiveTime -= Time.deltaTime;
        if (LiveTime < 0)
            BulletDestroy();
    }

    void BulletDestroy()
    {
        Destroy(gameObject);
    } 
}
