using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gun : MonoBehaviour
{
    //protected Bullet bullet;
    public GameObject bullet;

    public int clipSize = 30;
    public float bulletLoadingTime = 0.1f;
    public float rechargeMagazineTime = 5f;

    float timeBeforeTheShot = 0;
    int bulletInGun;

    void Start()
    {
        bulletInGun = clipSize;
    }

    public void Shoot()
    {
        if (timeBeforeTheShot < 0)
        {
            Shot();
            if (bulletInGun == 0)
                RechargeMagazine();
        }
    }

    void Shot()
    {
        GameObject newBullet = Instantiate(bullet.gameObject, transform.position, transform.rotation);
        newBullet.GetComponent<Rigidbody>().velocity = transform.right * bullet.GetComponent<Bullet>().Velosity;
        timeBeforeTheShot = bulletLoadingTime;
        bulletInGun--;
    }

    void RechargeMagazine()
    {
        timeBeforeTheShot = rechargeMagazineTime;
        bulletInGun = clipSize;
    }

    void Update()
    {
        timeBeforeTheShot -= Time.deltaTime;
    }
}
