using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    public Slider slider;

    public void SetMaxHealth(float health) { slider.maxValue = slider.value = health; }

    public float HP
    {
        set { slider.value = value; }
        get { return slider.value; }
    }
}
